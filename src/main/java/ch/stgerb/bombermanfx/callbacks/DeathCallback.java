package main.java.ch.stgerb.bombermanfx.callbacks;

import main.java.ch.stgerb.bombermanfx.board.Block;
import main.java.ch.stgerb.bombermanfx.board.Goomba;

public interface DeathCallback
{
    void onPlayerDeath();

    void onGoombaDeath(Goomba goomba, Block block);

    void onExplosionUpgradeDeath(Block block);

    void onBombUpgradeDeath(Block block);
}