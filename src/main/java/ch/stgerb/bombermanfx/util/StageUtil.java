package main.java.ch.stgerb.bombermanfx.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class StageUtil
{
    public void changeFxml(Stage stage, String fxml) throws IOException
    {
        configureStage(stage);
        Parent root = FXMLLoader.load(getClass().getResource(fxml));
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void setBorderPane(Stage stage, BorderPane borderPane)
    {
        configureStage(stage);
        stage.setScene(new Scene(borderPane));
        stage.show();
    }

    public void configureStage(Stage stage)
    {
        stage.setWidth(1088);
        stage.setHeight(925);
        stage.setResizable(false);
    }

    public void newStage(String fxml, String title) throws IOException
    {
        Stage stage = new Stage();
        stage.setTitle(title);
        configureStage(stage);
        changeFxml(stage, fxml);
    }
}