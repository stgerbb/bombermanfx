package main.java.ch.stgerb.bombermanfx.board;

import javafx.scene.layout.Pane;

public class Board extends Pane
{
    private int[][] level;
    private Block[][] blocks = new Block[14][17];
    private Player player;
    private Goomba goomba1;
    private Goomba goomba2;
    private Goomba goomba3;

    public Board(int[][] level, Player player, Goomba goomba1, Goomba goomba2, Goomba goomba3)
    {
        this.level = level;
        this.player = player;
        this.goomba1 = goomba1;
        this.goomba2 = goomba2;
        this.goomba3 = goomba3;

        for (int x = 0; x < level.length; x++)
        {
            for (int y = 0; y < level[x].length; y++)
            {
                int texture = level[x][y];
                if (texture == Texture.BRICKS.getValue())
                {
                    Block bricks = new Block("/main/resources/images/bricks.png", false, true, false, false, y, x);
                    blocks[x][y] = bricks;
                    getChildren().add(bricks);
                }
                else if (texture == Texture.WEED.getValue())
                {
                    Block weed = new Block("/main/resources/images/gras.png", true, false, false, false, y, x);
                    blocks[x][y] = weed;
                    getChildren().add(weed);
                }
                else if (texture == Texture.STONE.getValue())
                {
                    Block stone = new Block("/main/resources/images/stone.png", false, false, false, true, y, x);
                    blocks[x][y] = stone;
                    getChildren().add(stone);
                }
                else if (texture == Texture.WOOD.getValue())
                {
                    Block wood = new Block("/main/resources/images/wood.png", false, false, true, false, y, x);
                    blocks[x][y] = wood;
                    getChildren().add(wood);
                }
            }
        }

        getChildren().add(player);

        if (goomba1 != null)
        {
            getChildren().add(goomba1);
        }

        if (goomba2 != null)
        {
            getChildren().add(goomba2);
        }

        if (goomba3 != null)
        {
            getChildren().add(goomba3);
        }
    }

    public int[][] getLevel()
    {
        return level;
    }

    public Player getPlayer()
    {
        return player;
    }

    public Block getBlock(int row, int colum)
    {
        return blocks[row][colum];
    }

    public Goomba getGoomba1()
    {
        return goomba1;
    }

    public Goomba getGoomba2()
    {
        return goomba2;
    }

    public Goomba getGoomba3()
    {
        return goomba3;
    }
}
