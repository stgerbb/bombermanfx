package main.java.ch.stgerb.bombermanfx.board;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

public class Player extends Rectangle
{
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;

    private int row;
    private int column;
    private int plantedBombsCount = 0;
    private int maxPlaceableBombsCount = 1;
    private int explosionRadius = 2;

    public Player(String path, double x, double y, int row, int column)
    {
        this.row = row;
        this.column = column;

        Image player = new Image(path);

        setWidth(WIDTH);
        setHeight(HEIGHT);

        setX(x);
        setY(y);

        setFill(new ImagePattern(player));
    }

    public void setExplosionRadius(int radius)
    {
        explosionRadius = radius;
    }

    public void setMaxPlaceableBombsCount(int count)
    {
        maxPlaceableBombsCount = count;
    }

    public int getColumn()
    {
        return column;
    }

    public int getExplosionRadius()
    {
        return explosionRadius;
    }

    public void incExplosionRadius()
    {
        explosionRadius++;
    }

    public int getRow()
    {
        return row;
    }

    public void decRow()
    {
        row--;
    }

    public void decColumn()
    {
        column--;
    }

    public void incRow()
    {
        row++;
    }

    public void incColumn()
    {
        column++;
    }

    public int getPlantedBombsCount()
    {
        return this.plantedBombsCount;
    }

    public int getMaxPlaceableBombsCount()
    {
        return this.maxPlaceableBombsCount;
    }

    public void incPlantedBombsCount()
    {
        this.plantedBombsCount++;
    }

    public void decPlantedBombsCount()
    {
        this.plantedBombsCount--;
    }

    public void incMaxPlaceableBombsCount()
    {
        this.maxPlaceableBombsCount++;
    }
}