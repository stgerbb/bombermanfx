package main.java.ch.stgerb.bombermanfx.board;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

public class Explosion extends Rectangle
{
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;

    private String path;

    public Explosion(String path, double x, double y)
    {
        Image explosion = new Image(path);

        setWidth(64);
        setHeight(64);

        setX(x);
        setY(y);

        setFill(new ImagePattern(explosion));
    }
}