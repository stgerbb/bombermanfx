package main.java.ch.stgerb.bombermanfx.board;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

public class Goomba extends Rectangle
{
    private static final int WIDTH = 64;
    private static final int HEIGHT = 64;

    private String path;
    private int identifier;
    private int row;
    private int column;
    private boolean alive;

    public Goomba(String path, double x, double y, int row, int column, int identifier, boolean alive)
    {
        this.row = row;
        this.column = column;
        this.identifier = identifier;
        this.alive = alive;

        Image goomba = new Image(path);

        setWidth(WIDTH);
        setHeight(HEIGHT);

        setX(x);
        setY(y);

        setFill(new ImagePattern(goomba));
    }

    public void incRow()
    {
        row++;
    }

    public void decRow()
    {
        row--;
    }

    public void incColumn()
    {
        column++;
    }

    public void decColumn()
    {
        column--;
    }

    public int getIdentifier()
    {
        return identifier;
    }

    public int getRow()
    {
        return row;
    }

    public int getColumn()
    {
        return column;
    }

    public boolean isAlive()
    {
        if (alive)
        {
            return true;
        }
        return false;
    }

    public void setAlive(boolean bool)
    {
        alive = bool;
    }
}