package main.java.ch.stgerb.bombermanfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/main/resources/fxml/main_menu.fxml"));
        stage.setTitle("BombermanFX");
        stage.setScene(new Scene(root));
        stage.setWidth(1088);
        stage.setHeight(925);
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception
    {
//        Not in use
    }
}