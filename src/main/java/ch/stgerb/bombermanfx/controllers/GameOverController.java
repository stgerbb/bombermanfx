package main.java.ch.stgerb.bombermanfx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;
import main.java.ch.stgerb.bombermanfx.util.StageUtil;

import java.io.File;
import java.io.IOException;

public class GameOverController
{
    private StageUtil stage = new StageUtil();

    public GameOverController()
    {
        File file1 = new File("files/goomba.txt");
        if (file1.exists())
        {
            file1.delete();
        }

        File file2 = new File("files/level.txt");
        if (file2.exists())
        {
            file2.delete();
        }

        File file3 = new File("files/map.txt");
        if (file3.exists())
        {
            file3.delete();
        }

        File file4 = new File("files/player.txt");
        if (file4.exists())
        {
            file4.delete();
        }

        File file5 = new File("files/spawned_items.txt");
        if (file5.exists())
        {
            file5.delete();
        }

        File file6 = new File("files/time.txt");
        if (file6.exists())
        {
            file6.delete();
        }
    }

    @FXML
    public void onClickBackToMainMenu(ActionEvent event) throws IOException
    {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.changeFxml(s, "/main/resources/fxml/main_menu.fxml");
    }
}