package main.java.ch.stgerb.bombermanfx.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;
import main.java.ch.stgerb.bombermanfx.util.StageUtil;

import java.io.IOException;

public class ControlsController
{
    private StageUtil stage = new StageUtil();

    @FXML
    public void onClickReturnToMainMenu(ActionEvent event) throws IOException
    {
        Stage s = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.changeFxml(s, "/main/resources/fxml/main_menu.fxml");
    }
}